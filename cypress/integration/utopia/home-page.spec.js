// homepage.spec.js created with Cypress
//
// Start writing your Cypress tests below!
// If you're unfamiliar with how Cypress works,
// check out the link below and learn how to write your first test:
// https://on.cypress.io/writing-first-test

const HOMEPAGE_BANNER = '[data-cy=homepage-banner-title]'
const FILMMAKERS_NEW_PROJECT_PAGE = "/filmmakers/new-project"
const FILMMAKERS = "/filmmakers"

describe('Home page', () => {
    context('Authorized user click UPLOAD YOUR FILM on web', () => {
        beforeEach(() => {
            const FETCH_HOME_API = `${Cypress.env("apiUrl")}/fetch/home`
            cy.intercept('/payment/source/validate').as('getPaymentValidateRequest')
            cy.intercept('/fetch/home?size=16&fmsize=4').as('getFeaturesOnHomeRequest')

            cy.fixture('accounts.json').as('accounts')
            cy.get('@accounts').then(({ filmmaker }) => cy.login(filmmaker))
            cy.wait('@getFeaturesOnHomeRequest').its('response.statusCode').should('eq', 200)
        })

        it(`Should show add new content form`, () => {
            cy.get(HOMEPAGE_BANNER)
                .click()
            cy.wait('@getPaymentValidateRequest').its('response.statusCode').should('eq', 200)

            cy.url().should('include', FILMMAKERS_NEW_PROJECT_PAGE)
            cy.get('[data-cy=project-title]')
                .should('be.visible')
        })

        it('Show /filmmaker with button OPEN DASHBOARD on mobile screen', () => {
            cy.fixture('devices.json').then(({ mobileDevices }) => {
                mobileDevices.forEach((size) => {
                    if (Cypress._.isArray(size)) {
                        cy.viewport(size[0], size[1])
                    } else {
                        cy.viewport(size)
                    }
                    // Click UPLOAD YOUR FILM
                    cy.get('[data-cy=homepage-banner-title]')
                        .click()
                    // Check message “switch to desktop browser” on /filmmaker 
                    cy.url().should('include', FILMMAKERS)
                    cy.get('[data-cy=get-started-now]')
                        .should('contain.text', 'OPEN DASHBOARD')
                        .and('be.visible')
                    cy.go('back')
                })
            })
        })
    })

    context('Unauthorized user', () => {
        it('Should redirect to login when unauthorized user clicks on Upload Your Film', () => {
            cy.visitWithAuth('/')
            // Give this element 5 seconds to appear. The default is 4
            cy.get('[data-cy=homepage-banner-desc]', { timeout: 5000 })
                .should('be.visible')
                .and('contain.text', 'SELL your film,')
                .and('contain.text', 'GROW your audience, and stay CONNECTED.')

            cy.get(HOMEPAGE_BANNER)
                .should('contain', 'UPLOAD YOUR FILM!')
                .click()
            cy.url().should('include', '/log-in')
        })

        it('Demo debug', () => {
            let actualText = undefined // evaluates immediately as undefined
          
            cy.visitWithAuth('/', {failOnStatusCode:true}) // Nothing happens yet
            cy.get(HOMEPAGE_BANNER).debug() // Still, nothing happens yet
              .then(($el) => {
                // Nothing happens yet
                // this line evaluates after the .then() executes
                actualText = $el.text()
                // evaluates after the .then() executes
                // it's the correct value gotten from the $el.text()
                if (actualText == 'UPLOAD YOUR FILM!') {
                  cy.contains(actualText).click()
                  cy.url().should('include', '/log-in')
                } else {
                  cy.contains('For Filmmakers').click()
                }
              })
          })
    })
})