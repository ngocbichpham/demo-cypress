// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add('login', (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add('drag', { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add('dismiss', { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite('visit', (originalFn, url, options) => { ... })
Cypress.Commands.add("visitWithAuth", (path) => {
    cy.visit(`${path}`, {
        auth: {
            username: 'devel',
            password: 'devel_pass',
        }
    })
})

Cypress.Commands.add('login', (user) => {
    const LOGIN_API = '/account/log-in'
    const LOGIN_PAGE = '/log-in'
    const { email, password } = user

    cy.intercept('POST', LOGIN_API).as('login')
    cy.intercept('/user/*').as('getUserInformation')
    cy.intercept('/permissions/projects/existed').as('isUserHasProjects')

    cy.visitWithAuth(LOGIN_PAGE)
    cy.get('[data-cy=login-email]')
        .type(email)
        .should('have.value', email)

    cy.get('[data-cy=login-password]')
        .type(password)
        .should('have.value', password)

    cy.get('[data-cy=login-submit]').click()

    cy.wait('@login').then(interception => {
        expect(interception.response.statusCode).to.equal(200)
        Cypress.env('token', interception.response.body.authToken)
        cy.log(Cypress.env('token'))
    })
    cy.wait('@getUserInformation').its('response.statusCode').should('eq', 200)
    cy.wait('@isUserHasProjects').its('response.statusCode').should('eq', 200)
})

Cypress.Commands.add('loginByApi', (user) => {
    const LOGIN_API = '/account/log-in'
    const { email, password } = user

    const options = {
        method: 'POST',
        url: LOGIN_API,
        body: {
            email: email,
            password: password,
            type: "EmailCredential"
        },
    };

    cy.request(options).then(response => {
        expect(response.status).to.equal(200)
        Cypress.env('token', response.body.authToken)
        cy.log(Cypress.env('token'))
    })
})

Cypress.Commands.add(
    "getProjectConvertFail",
    () => {
        let token = Cypress.env('token')
        const authorization = `Bearer ${token}`;
        let lstAvailableMovies = []
        let finalList = []
        // get just add movie
        getJustAddedMovies(token).then(response => {
            // Get list movies under Just Added
            var from = new Date('2021-03-01')
            var to = new Date('2021-04-15')
            // lstAvailableMovies = response.body.data.results.filter(movie => new Date(movie.publishedAt.replace('[UTC]', '')) >= from && new Date(movie.publishedAt.replace('[UTC]', '')) <= to)
            //     .map(movie => movie.projectId)

            // // lstAvailableMovies = response.body.data.results.filter(movie => movie.slug =='2xtntc0v-2jkq-m1')
            // // .map(movie => movie.projectId)
            // cy.log(lstAvailableMovies)

            // lstAvailableMovies = response.body.data.results

            // cy.log(lstAvailableMovies)
            lstAvailableMovies = response.body.data.results
            lstAvailableMovies.forEach(currentProject => {
                const optionsDraftProject = {
                    method: 'GET',
                    url: getEndpoint(`/project/${currentProject.projectId}/draft`),
                    failOnStatusCode: false,
                    headers: {
                        authorization,
                    }
                };

                cy.request(optionsDraftProject, {timeout : 6000}).then(response => {
                    var projectInformation = response.body
                        if(Object.keys(projectInformation).includes('created')) {
                            const isMissingInformation = new Date(projectInformation.created.replace('[UTC]', '')) >= from && new Date(projectInformation.created.replace('[UTC]', '')) <= to
                            if (isMissingInformation) {
                                cy.log(currentProject.projectId)
                                finalList.push(currentProject.projectId)
                                cy.log(finalList)
                            } 
                        }
                   
                })
            })
        })
    }
)